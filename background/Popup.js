class Popup{

	static onCreated(tab) {
	  console.log(`Created new tab: ${tab.id}`)
	}

	static onError(error) {
	  console.log(`Error: ${error}`);
	}

	static handle(){
		var creating = browser.tabs.create({ url:"/resources/explorer.html", active:true });
	    creating.then(Popup.onCreated, Popup.onError);
	}
	
}