class ItemStorage {

    constructor(){
        this.storage = browser.storage.sync; // Could be local|sync|managed
        this.storage_name = "collectedItems";
    }

    save(item) {

        var me = this;
        return new Promise((resolve, reject) => {

          me.getData().then(function(data){

            if(data.collectedItems == undefined)
              data.collectedItems = [];

            // If it exists, it is updated
            var matchingItem = data.collectedItems.filter(storedItem => storedItem && storedItem.url == item.url);
            if(matchingItem && matchingItem.length > 0){
                var index = data.collectedItems.map(e => e && e.url).indexOf(item.url); 
                data.collectedItems[index] = item;
            }
            else { // Otherwise it is added
                data.collectedItems.push(item);
            }    
            me.storage.set(data);        

            resolve(data.collectedItems);

          });
        });
    }

    delete(id){
        var me = this;
        return new Promise((resolve, reject) => {
            this.getData().then(function(data){
                var found = data.collectedItems.findIndex(function(item) {
                    console.log(id, item)
                  return item && item.url && item.url == id;
                });
                data.collectedItems.splice(found, 1);
                me.storage.set(data);
                resolve();
            });
        });
    }

    clearAll(){
        return new Promise((resolve, reject) => {
            this.storage.clear().then(function(){
                resolve();
            });
        });
    }

    getData(){
        return new Promise((resolve, reject) => {
            this.storage.get("collectedItems").then(function(data) {
                resolve(data);
            });
        });
    }

    getItems(){
        return new Promise((resolve, reject) => {
            this.getData().then(function(data){
                if (data.collectedItems) {
                    resolve(data.collectedItems);
                }else{
                    resolve([]);
                }
            });
        });
    }

    getItem(url){
        return new Promise((resolve, reject) => {
            this.getItems().then(function(collectedItems){
                resolve(collectedItems.filter(item => item && item.url == url)[0])
            });
        });
    }

}