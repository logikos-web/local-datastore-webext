class DssStorage {

  constructor(){
      this.storage = browser.storage.sync; // Could be local|sync|managed
      this.storage_name = "collectedItems";
  }

  getModelByKey(key) {

    return new Promise((resolve, reject) => {

      resolve(this._getCultivatorModel()); //model
    });
  }
  getModelNames() {

    return new Promise((resolve, reject) => {
      var models = [];
          models.push(this._getCultivatorModel());
          models.push(this._getTomatoSeedsModel());
          models.push(this._getSeedDrillModel());
      resolve(models);
    });
  }
  _getCultivatorModel(){
      return {
        description: "This model was designed to help you picking a good cultivator for medium-size tomato farms.",
        urlBase: "http://localhost:8080/ahp-api/sharedModels/Buying%20a%20cultivator",
        objectType: "https://www.w3.org/2002/07/owl#Thing",
        title: "Buying a cultivator",
        goal: {
          nodes: [
            {
              smartJudgementStrategy: { strategy: "NormalizedDifference" },
              name: "Price",
              overallPriority: 0.5399262899262899,
              localPriority: 0.5399262899262899
            },
            {
              smartJudgementStrategy: { strategy: "NormalizedDifference" },
              name: "Tillage depth",
              overallPriority: 0.14255733005733004,
              localPriority: 0.14255733005733004
            },
            {
              smartJudgementStrategy: { strategy: "NormalizedDifference" },
              name: "Tillage width",
              overallPriority: 0.25772932022932027,
              localPriority: 0.25772932022932027
            },
            {
              smartJudgementStrategy: {
                model: {
                  nodes: [
                    {
                      localPriority: 0.22666222666222668,
                      name: "Belt",
                      overallPriority: 0.22666222666222668
                    },
                    {
                      localPriority: 0.07181707181707181,
                      name: "Gears",
                      overallPriority: 0.07181707181707181
                    },
                    {
                      localPriority: 0.7015207015207015,
                      name: "Chain",
                      overallPriority: 0.7015207015207015
                    }
                  ],
                  name: "Goal"
                },
                strategy: "CompleteDomainPriorization"
              },
              name: "Transmission",
              overallPriority: 0.05978705978705978,
              localPriority: 0.05978705978705978
            }
          ], //nodes
          name: "Goal"
        } //goal
      }
  }
  _getTomatoSeedsModel(){
    return {
        description: "This decision model allows you to pick seeds of tomato for the area of La Plata. It is based in a survey made by the students of Agronomy of the University.",
        urlBase: "http://localhost:8080/ahp-api/sharedModels/Buying%20a%20cultivator",
        objectType: "https://www.w3.org/2002/07/owl#Thing",
        title: "Buying tomato seeds for Bs As",
        goal: {
          nodes: [
            {
              smartJudgementStrategy: { strategy: "NormalizedDifference" },
              name: "Variety",
              overallPriority: 0.7,
              localPriority: 0.7
            },
            {
              smartJudgementStrategy: { strategy: "NormalizedDifference" },
              name: "Hours after seed isolation",
              overallPriority: 0.1,
              localPriority: 0.1
            },
            {
              smartJudgementStrategy: { strategy: "NormalizedDifference" },
              name: "Price",
              overallPriority: 0.2,
              localPriority: 0.2
            }
          ], //nodes
          name: "Goal"
        } //goal
      }
  }
  _getSeedDrillModel(){
    return {
        description: "This is a model for choosing a seed grill for the ground of Santa Fé for soy plantation.",
        urlBase: "http://localhost:8080/ahp-api/sharedModels/Buying%20a%20cultivator",
        objectType: "https://www.w3.org/2002/07/owl#Thing",
        title: "Buying a seed drill",
        goal: {
          nodes: [
            {
              smartJudgementStrategy: { strategy: "NormalizedDifference" },
              name: "Price",
              overallPriority: 0.7,
              localPriority: 0.7
            },
            {
              smartJudgementStrategy: { strategy: "NormalizedDifference" },
              name: "Spacing",
              overallPriority: 0.2,
              localPriority: 0.2
            }
          ], //nodes
          name: "Goal"
        } //goal
      }
  }
}