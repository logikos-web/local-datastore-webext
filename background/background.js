/**
 * Create a BackgroundFacade and delegate everything to it.
 * The BackgroundFacade is the interface between the external, non-OO world 
 * and our object oriented background subsystem.
 */

var facade = BackgroundFacade.getSingleton();

// listen for messages from same extension.
browser.runtime.onMessage.addListener(rmcRequest => { 
	return facade.handle(rmcRequest)
});

// listen for messages from another extension.
browser.runtime.onMessageExternal.addListener(rmcRequest => {
    return facade.handle(rmcRequest);
});

// listen for icon user click 
browser.browserAction.onClicked.addListener(tab => { Popup.handle(tab); });