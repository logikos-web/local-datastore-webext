var dssService = new DssStorage();

$(document).ready(function () {

    $('#profileOptions').on('click','#configureProfile', function (event) {
    	event.preventDefault();
    	configureProfile();
    });

	function configureProfile(){
		var profileValue = document.querySelector("#profile").value;

		if(profileValue != 0){
			loadProfileSection();
			loadMatchingProfileConfig(profileValue);
		}
		else{
			alert("Please, choose a profile from the combo first.")
		}
	}

	function showProfiles(){

		dssService.getModelNames().then(models => {

			var select = document.querySelector("#profile");
			models.forEach(model => {
				var opt = document.createElement("option");
					opt.text = model.title;
					opt.value = model.title;
					opt.model = model;
				select.add(opt);
			});
			select.onchange = function(evt){
				var model = select[select.selectedIndex].model;
				updateProfileDescription(model.description);
				showPieChart(model);
				window.sortItems(model);
			};
		});
	}

	function updateProfileDescription(description){
		document.querySelector("#profile-description").innerHTML = description;
	}

	function showPieChart(model){
		console.log("DATA", model);
		document.querySelector(".chart-container").innerHTML = '<canvas id="profileChart"></canvas>';
						
		var ctx = document.querySelector("#profileChart").getContext('2d');
		var labels = [];
		var data = [];

		model.goal.nodes.forEach(node => {
			labels.push(node.name);
			data.push(node.overallPriority);
		});

		var myChart = new Chart(ctx, {
		  type: 'pie',
		  options: {
			responsive: true,
			maintainAspectRatio: false,
			labels: {
		        generateLabels: (chart) => {

		          chart.legend.afterFit = function () {
		            var width = this.width; 
		            console.log(this);
		           
		            this.lineWidths = this.lineWidths.map( () => this.width-12 );
		            
		            this.options.labels.padding = 30;
		            this.options.labels.boxWidth = 15;
		          };

		          var data = chart.data;
		          //https://github.com/chartjs/Chart.js/blob/1ef9fbf7a65763c13fa4bdf42bf4c68da852b1db/src/controllers/controller.doughnut.js
		          if (data.labels.length && data.datasets.length) {
		            return data.labels.map((label, i) => {
		              var meta = chart.getDatasetMeta(0);
		              var ds = data.datasets[0];
		              var arc = meta.data[i];
		              var custom = arc && arc.custom || {};
		              var getValueAtIndexOrDefault = this.getValueAtIndexOrDefault;
		              var arcOpts = chart.options.elements.arc;
		              var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
		              var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
		              var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
		              
		              return {
		                text: label,
		                fillStyle: fill,
		                strokeStyle: stroke,
		                lineWidth: bw,
		                hidden: isNaN(ds.data[i]) || meta.data[i].hidden,

		                // Extra data used for toggling the correct item
		                index: i
		              };
		            });
		          }
		          return [];
		        }
			}
		  },
		  data: {
		    labels: labels,
		    datasets: [{
		      backgroundColor: ["#2ecc71", "#3498db", "#95a5a6", "#9b59b6", "#f1c40f", "#e74c3c", "#34495e"],
		      data: data
		    }]
		  }
		});
	}

	function loadProfileSection(){
		document.querySelector("#collectedAlternatives").style.display = "none";
		document.querySelector("#profileConfiguration").style.display = "";
	}

	function loadMatchingProfileConfig(profile){
		dssService.getModelByKey(profile).then(model => {
			document.querySelector("#profileConfiguration").innerHTML = JSON.stringify(model, null, 1);
		});
	}

	showProfiles(); //document.querySelector("#profileConfiguration").style.display = "none";
});