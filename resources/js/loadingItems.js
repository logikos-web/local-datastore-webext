var storage = new ItemStorage();
var items = [];


$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

    $('#clear-btn').on('click', function (event) {
    	event.preventDefault();
        storage.clearAll().then(function(){
        	UpdateData();
        	$('#clearModal').modal('hide');
        });
    });   
		
	$('#clear-modal-btn').on('click', function (event) {
    	event.preventDefault();
		$('#clearModal').modal()
	});

	$('#close-btn').on('click', function (event) {
    	window.close();
    });   

    $('#export-btn').on('click', function (event) {
    	event.preventDefault();
    	var json =  JSON.stringify(items,null,4) ;
    	var blob = new Blob([json], {type: "application/json"});
		var url = URL.createObjectURL(blob);
		browser.downloads.download({
		 	filename: 'storage.json',
		  	url: url // The object URL can be used as download URL 
		});
    });   

    $('#list-items').on('click','.delete-btn', function (event) {
    	event.preventDefault();
        storage.delete(event.target.id).then(function(){
        	UpdateData();
        });  
    });

    $('#list-items').on('click','.edit-btn', function (event) {
    	event.preventDefault();
        editData(event.target.id);
    });

    $('#editItemModal').on('click','.btn-save-item', function (event) {
    	event.preventDefault();

        var properties = {};
        var domProperties = document.querySelectorAll("[id^='item-property-']");

        domProperties.forEach(domProp =>{
        	properties[domProp.id.replace('item-property-','')] = domProp.value;
        });

        var obj = {
        	"type": document.querySelector("#item-type").value,
        	"url": document.querySelector("#item-url").value,
        	"properties": properties
        };

        storage.save(obj);
        $('#editItemModal').modal('hide');
    });

    function editData(itemId){
		getItemData(itemId).then(item => {

			var modalBody = document.querySelector("#editItemModal .modal-body")
			loadFields(item, modalBody);
		})
		
	}

	function getItemData(itemUrl){
		return new Promise((resolve, reject) => {
			storage.getItem(itemUrl).then(foundItem => resolve(foundItem));
		});
	}

	function loadFields(item, domElem){

		if(item == undefined)
			return;
		
		domElem.innerHTML = "";

		appendInput("item-type", "Type", item.type, domElem);
		appendInput("item-url", "URL", item.url, domElem);

		for (var key in item.properties) {
			appendInput("item-property-" + key, key, item.properties[key], domElem);
		}

	}

	window.sortItems = function(model){
		console.log(model);
		items = items.sort((a, b) => toNumber(a.properties.price) - toNumber(b.properties.price));
		showData(items);
	}

	function toNumber(str){
		return parseFloat(str.replace(/\D/g,''))
	}

	function appendInput(id, label, value, container){
		
		var group = document.createElement("div"); 
			group.innerHTML = `
				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <span class="input-group-text">${label}</span>
				  </div>
				  <input id="${id}" value="${value.replace(/\s+/g,' ').trim()}" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
				</div>`;

		container.appendChild(group);
	}

	function showData(data){

		//document.querySelector("#loading").classList.remove("not-loading");
		document.querySelector("#collectedAlternatives").style.display = "";
	
		if (data.length == 0) { 
			document.getElementById("list-items").innerHTML = `<p>No data to show</p>`; }
		else{
			document.getElementById("list-items").innerHTML = ``; 
			items = data.filter(item => item != null);
		}
		data.forEach(function(element) {
			if(element == undefined)
				return;

			var newDiv = document.createElement("div"); 
			newDiv.className += " card";

			var propsAsString = "{</br>";
			for (var key in element.properties) {

				var formattedPropValue = element.properties[key].replace(/\s+/g,' ').trim();
					formattedPropValue = formattedPropValue.length > 50? formattedPropValue.substring(0,50) + " [...]" : formattedPropValue;					

				propsAsString = propsAsString + '	"' + key + '": ' + '"' + formattedPropValue + '",' + "</br>";
			}
			propsAsString = propsAsString.substring(0, propsAsString.length - 6) + "</br>}";

			newDiv.innerHTML = `
			<div class="card-body" >
				<h5 class="card-title">${element.url}</h5>
				<pre class="m-0 card-text"><code>${propsAsString}</code></pre>
				<br>
				<span class="badge badge-secondary float-right">${element.type}</span>
				<button id="${element.url}" class="delete-btn btn btn-danger"><i class="fas fa-times-circle"></i> Delete</button> 
				<button id="${element.url}" type="button" data-toggle="modal" data-target="#editItemModal" class="edit-btn btn btn-danger"><i class="fas fa-edit"></i> Edit</button> 
			</div>`;

			document.getElementById("list-items").append(newDiv);
			$(newDiv).hide().slideDown({duration: 200, queue:false});
			//document.querySelector("#loading").classList.add("not-loading");
		});

		//document.querySelector("#loading").style.display = "none";
	}

	function UpdateData(){
		document.getElementById("list-items").innerHTML = `<p>No data to show</p>`; 
		storage.getItems().then(function(data){
			showData(data);
		});
	}


	browser.storage.onChanged.addListener(function(){
		UpdateData();
	});

	storage.getItems().then(function(data){
		showData(data);
	});

});

// Test saving something
// arg = { itemType:'video', uri:'http://algoestamal.com', properties: {title:"hello darkness"}}
// storage.save(arg);
